package test.order.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by huang on 2018/2/6.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderApiTests {

    @Autowired
    OrderApi orderApi ;

    @Test
    public void testCreateOrder(){
        orderApi.createOrder(null);
    }
}
