package test.web.service;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;
import test.api.common.RequestVo;
import test.api.common.ResponseVo;
import test.order.api.OrderApi;
import test.order.api.vo.OrderVo;

/**
 * Created by huang on 2018/2/6.
 */

@Service
public class TestService {

    @Reference
    OrderApi orderApi;

    public OrderVo createOrder(OrderVo orderVo) {
        RequestVo<OrderVo> requestVo = new RequestVo<OrderVo>();
        requestVo.setRequest(orderVo);
        ResponseVo<OrderVo> responseVo= orderApi.createOrder(requestVo);
        return responseVo.getResponse();
    }
}
