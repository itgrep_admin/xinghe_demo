package test.order.config;

import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;

/**
 * Created by huang on 2018/2/6.
 */
@Configuration
@EnableJms
public class JmsConfig {


    @Bean
    MessageConverter messageConverter (){
        MappingJackson2MessageConverter jackson2MessageConverter = new MappingJackson2MessageConverter();
        jackson2MessageConverter.setTypeIdPropertyName("classname");
        return jackson2MessageConverter;
    }


    @Autowired
    public void setActiveMQConnectionFactory(PooledConnectionFactory activeMQConnectionFactory){
        RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
        redeliveryPolicy.setMaximumRedeliveries(0);

//        ((ActiveMQConnectionFactory)activeMQConnectionFactory.getConnectionFactory()).setRedeliveryPolicy(redeliveryPolicy);
//        activeMQConnectionFactory.setRedeliveryPolicy(redeliveryPolicy);
    }
}
