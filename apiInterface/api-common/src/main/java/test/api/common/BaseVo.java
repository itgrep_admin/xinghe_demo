package test.api.common;

import java.io.Serializable;

/**
 * Created by huang on 2018/2/2.
 */
public abstract class BaseVo implements Serializable {

    private Integer resultCode;

    private String message;

    private String errorMessage;

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
