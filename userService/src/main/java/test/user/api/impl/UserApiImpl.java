package test.user.api.impl;

import com.weibo.api.motan.config.springsupport.annotation.MotanService;
import org.springframework.transaction.annotation.Transactional;
import test.api.common.RequestVo;
import test.api.common.ResponseVo;
import test.user.api.UserApi;
import test.user.api.vo.UserVo;

/**
 * Created by huang on 2018/2/5.
 */
@MotanService
@Transactional
public class UserApiImpl implements UserApi {
    @Override
    public ResponseVo<UserVo> userInfo(RequestVo<UserVo> userRequestVo) {
        return null;
    }

    @Override
    public ResponseVo<UserVo> updateUser(RequestVo<UserVo> userRequestVo) {
        return null;
    }
}
