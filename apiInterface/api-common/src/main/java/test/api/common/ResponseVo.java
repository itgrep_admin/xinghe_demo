package test.api.common;

import java.io.Serializable;

/**
 * Created by huang on 2018/2/2.
 */
public class ResponseVo<T extends Serializable> extends BaseVo implements Serializable {

    private T response;

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
