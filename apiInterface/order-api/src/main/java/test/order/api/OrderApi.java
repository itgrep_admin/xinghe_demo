package test.order.api;

import test.api.common.RequestVo;
import test.api.common.ResponseVo;
import test.order.api.vo.OrderVo;

/**
 * Created by huang on 2018/2/6.
 * 订单服务
 */
public interface OrderApi {

    ResponseVo<OrderVo> orderInfo(String orderNo);

    ResponseVo<OrderVo> createOrder(RequestVo<OrderVo> requestVo);

    ResponseVo<OrderVo> updateOrder(RequestVo<OrderVo> requestVo);
}
