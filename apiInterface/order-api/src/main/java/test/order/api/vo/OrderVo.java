package test.order.api.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by huang on 2018/2/6.
 */
public class OrderVo implements Serializable {

    private String orderId;

    private String productId;

    private String productName;

    private Date createdDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
