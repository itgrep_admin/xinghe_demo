package test.order.config;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.context.annotation.Configuration;

/**
 * Created by huang on 2018/2/6.
 */
@Configuration
//@DubboComponentScan(basePackages = "test.order.api.impl")
@EnableDubboConfiguration
public class DubboConfiguration {

  /*  @Bean
    public ApplicationConfig applicationConfig() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("provider-test");
        return applicationConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress("zookeeper://127.0.0.1:2181");
        registryConfig.setClient("curator");
        return registryConfig;
    }*/
}
