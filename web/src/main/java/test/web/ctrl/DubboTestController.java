package test.web.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.order.api.vo.OrderVo;
import test.web.service.TestService;

import java.util.Date;
import java.util.UUID;

/**
 * Created by huang on 2018/2/6.
 */

@RestController
@RequestMapping("test")
public class DubboTestController {

    @Autowired
    TestService testService;

    @RequestMapping("")
    public Object test(){
        OrderVo orderVo = new OrderVo();
        orderVo.setProductName("测试产品");
        orderVo.setProductId(UUID.randomUUID().toString());
        orderVo.setOrderId(UUID.randomUUID().toString());
        orderVo.setCreatedDate(new Date());
        return testService.createOrder( orderVo);
    }
}
