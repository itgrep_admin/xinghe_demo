package test.web.config;

import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.spring.boot.DubboProperties;
import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by huang on 2018/2/6.
 */
//@DubboComponentScan(basePackages = "test.web")
@EnableDubboConfiguration
@Configuration
public class DubboConfig {

    @Autowired
    private DubboProperties properties;

    @Bean
    public RegistryConfig dubboRegistryConfig() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(this.properties.getRegistry());
//        registryConfig.setRegister(false);
        return registryConfig;
    }
}
