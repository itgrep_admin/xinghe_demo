package test.user;

import com.weibo.api.motan.common.MotanConstants;
import com.weibo.api.motan.util.MotanSwitcherUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by huang on 2018/2/5.
 */
@SpringBootApplication
public class UserServiceApp {

    public static void main(String []args){
        SpringApplication.run(UserServiceApp.class,args);

        MotanSwitcherUtil.setSwitcherValue(MotanConstants.REGISTRY_HEARTBEAT_SWITCHER, true);
    }
}
