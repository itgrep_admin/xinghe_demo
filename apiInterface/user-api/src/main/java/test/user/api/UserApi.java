package test.user.api;

import test.api.common.RequestVo;
import test.api.common.ResponseVo;
import test.user.api.vo.UserVo;

/**
 * Created by huang on 2018/2/2.
 */
public interface UserApi {

    ResponseVo<UserVo> userInfo(RequestVo<UserVo> userRequestVo);

    ResponseVo<UserVo> updateUser(RequestVo<UserVo> userRequestVo);
}
