package test.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by huang on 2018/2/6.
 */
@SpringBootApplication
public class OrderServiceApp {

    public static void main(String []args){
        SpringApplication.run(OrderServiceApp.class,args);
    }
}
