package test.register;

import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Created by huang on 2018/2/6.
 *
 *  服务自动注册到zookeeper
 */
@Configuration
@ConditionalOnClass(ZkClient.class)
@EnableConfigurationProperties(ServerRegisterProperties.class)
public class ServerAutoRegisterAutoConfiguration {


    @Autowired
    private ServerRegisterProperties properties;

    @Bean
    ZkClient makeClient(){
        return new ZkClient(properties.getZkUrl(),properties.getSessionTimeout(),properties.getConnectionTimeout());
    }

    @PostConstruct
    void doRegister(){
        ZkRegister zkRegister = new ZkRegister(properties, makeClient());
        zkRegister.autoRegister();
    }


}
