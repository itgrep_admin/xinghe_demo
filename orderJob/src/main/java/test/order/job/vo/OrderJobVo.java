package test.order.job.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by huang on 2018/2/6.
 */
public class OrderJobVo implements Serializable{

    private String orderId;

    private Date createTime;

    private String orderStatus;

    private String productId;

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
