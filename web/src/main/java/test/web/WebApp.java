package test.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by huang on 2018/2/6.
 */
@SpringBootApplication
public class WebApp {

    public static void main(String []args){
        SpringApplication.run(WebApp.class,args);
    }
}
