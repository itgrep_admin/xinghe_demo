package test.api.common;

import java.io.Serializable;

/**
 * Created by huang on 2018/2/2.
 */
public class RequestVo<T extends Serializable> extends BaseVo implements Serializable {

    private T request;

    public T getRequest() {
        return request;
    }

    public void setRequest(T request) {
        this.request = request;
    }
}
