package test.order.job.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;

/**
 * Created by huang on 2018/2/6.
 */
@Configuration
//@EnableJms
public class JmsConfig {

    @Autowired
    public void setDefaultJmsListenerContainerFactory (DefaultJmsListenerContainerFactory  containerFactory){

    }


    @Bean
    MessageConverter messageConverter (){
        MappingJackson2MessageConverter jackson2MessageConverter = new MappingJackson2MessageConverter();
        jackson2MessageConverter.setTypeIdPropertyName("classname");
        return jackson2MessageConverter;
    }
   /*@Bean
   MessageConverter messageConverter(){
       return new MessageConverter() {
           @Override
           public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
               ActiveMQTextMessage activeMQTextMessage =new ActiveMQTextMessage() ;

               activeMQTextMessage.setText(JSON.toJSONString(object));

               return activeMQTextMessage;
           }

           @Override
           public Object fromMessage(Message message) throws JMSException, MessageConversionException {
               return null;
           }
       };
   }*/

    @Autowired
    public void setActiveMQConnectionFactory(PooledConnectionFactory activeMQConnectionFactory){
        RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
        redeliveryPolicy.setMaximumRedeliveries(0);

        ((ActiveMQConnectionFactory)activeMQConnectionFactory.getConnectionFactory()).setRedeliveryPolicy(redeliveryPolicy);
//        activeMQConnectionFactory.setRedeliveryPolicy(redeliveryPolicy);
    }
}
