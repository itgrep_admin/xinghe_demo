package test.order.job.jobservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import test.order.api.vo.OrderVo;

/**
 * Created by huang on 2018/2/6.
 */
@Component
public class OrderJob {

    private static final Logger logger = LoggerFactory.getLogger(OrderJob.class);

    int i =0;

    @JmsListener(destination = "order_created")
    public void processOrderCreateMessage(OrderVo message){

        logger.info("get a order created message :{}" , message);


    }

}
